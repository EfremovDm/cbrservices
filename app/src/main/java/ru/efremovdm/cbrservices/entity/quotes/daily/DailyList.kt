package ru.efremovdm.cbrservices.entity.quotes.daily

import org.simpleframework.xml.Root;
import org.simpleframework.xml.ElementList

/**
 * https://habr.com/post/116830/
 * https://www.javacodegeeks.com/2011/02/android-xml-binding-simple-tutorial.html
 */
@Root(name = "ValCurs", strict = false)
data class DailyList(
        @field:ElementList(name = "Valute", inline = true)
        var listOfDaily: List<DailyOne>? = null
)