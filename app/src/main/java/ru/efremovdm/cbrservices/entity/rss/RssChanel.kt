package ru.efremovdm.cbrservices.entity.rss

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "channel", strict = false)
class RssChanel {

    @field:Element(name = "title")
    var title: String = ""

    @field:Element(name = "description")
    var description: String = ""

    @field:Element(name = "copyright")
    var copyright: String = ""

    //@field:Element(name = "atom:link") // TODO: почему возникает ошибка?
    //var atomLink: String = ""

    //@field:Element(name = "link") // TODO: почему возникает ошибка?
    //var link: String = ""

    @field:Element(name = "language")
    var language: String = ""

    @field:Element(name = "lastBuildDate")
    var lastBuildDate: String = ""

    @field:Element(name = "ttl")
    var ttl: String = ""

    @field:ElementList(name = "item", inline = true)
    var itemList: List<RssItem>? = null
}