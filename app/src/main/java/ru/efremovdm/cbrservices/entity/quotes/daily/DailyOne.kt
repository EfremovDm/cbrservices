package ru.efremovdm.cbrservices.entity.quotes.daily

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root

//@Element(name = "ValCurs")
@Root(name = "Valute", strict = true)
class DailyOne {

    @get:Attribute(name = "ID")
    @set:Attribute(name = "ID")
    var id: String? = null

    @field:Element(name = "NumCode")
    var numCode: String? = null

    @field:Element(name = "CharCode")
    var charCode: String? = null

    @field:Element(name = "Nominal")
    var nominal: String? = null

    @field:Element(name = "Name")
    var name: String? = null

    @field:Element(name = "Value")
    var value: String? = null
}