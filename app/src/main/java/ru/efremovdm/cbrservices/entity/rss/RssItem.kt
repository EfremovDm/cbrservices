package ru.efremovdm.cbrservices.entity.rss

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
class RssItem {
    @field:Element(name = "title")
    var title: String = ""

    @field:Element(name = "link")
    var link: String = ""

    @field:Element(name = "guid")
    var guid: String = ""

    //@field:Element(name = "description") // TODO: почему возникает ошибка?
    //var description: String = ""

    @field:Element(name = "pubDate")
    var pubDate: String = ""

    @field:Element(name = "category")
    var category: String = ""
}