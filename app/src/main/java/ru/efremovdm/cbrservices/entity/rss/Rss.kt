package ru.efremovdm.cbrservices.entity.rss

import org.simpleframework.xml.Root;
import org.simpleframework.xml.Element

@Root(name = "rss", strict = false)
data class Rss(

    @field:Element(name = "channel")
    var channel: RssChanel? = null
)