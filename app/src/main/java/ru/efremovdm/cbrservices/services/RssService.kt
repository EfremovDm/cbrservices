package ru.efremovdm.cbrservices.services

import retrofit2.Call
import retrofit2.http.GET
import ru.efremovdm.cbrservices.entity.rss.Rss

interface RssService {

    // Новое на сайте
    @GET("/rss/RssNews")
    fun getRssNews(): Call<Rss>

    // Пресс-релизы Банка России
    @GET("/rss/RssPress")
    fun getRssPress(): Call<Rss>

    // Курсы валют, устанавливаемые ежедневно
    @GET("/scripts/RssCurrency.asp")
    fun getRssCurrency(): Call<Rss>

    // Информация выполнения регламента обработки электронных документов (МЦИ)
    @GET("/mcirabis/RssFeeds/MciRss.aspx")
    fun getRssMsi(): Call<Rss>

    // Новое на сайте (англ.)
    @GET("/eng/RssNews.aspx")
    fun getEnRssNews(): Call<Rss>

    // Пресс-релизы Банка России (англ.)
    @GET("/eng/RssPress.aspx")
    fun getEnRssPress(): Call<Rss>
}