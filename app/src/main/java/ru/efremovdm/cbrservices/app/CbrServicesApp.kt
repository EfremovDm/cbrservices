package ru.efremovdm.cbrservices.app

import android.app.Application
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import ru.efremovdm.cbrservices.services.RssService

class CbrServicesApp: Application() {

    val client = OkHttpClient()
    private var retrofit: Retrofit? = null

    private lateinit var rssService: RssService

    override fun onCreate() {
        super.onCreate()

        retrofit = Retrofit.Builder()
                .baseUrl("http://www.cbr.ru")
                .client(OkHttpClient()) //TODO: client
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build()
        rssService = retrofit!!.create(RssService::class.java)

        INSTANCE = this
    }
    
    companion object {
        lateinit var INSTANCE: CbrServicesApp
        val client by lazy { INSTANCE.client }

        fun getApi(): RssService {
            return INSTANCE.rssService
        }
    }
}