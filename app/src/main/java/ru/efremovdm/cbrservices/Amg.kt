package ru.efremovdm.cbrservices

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.View


class Amg(context: Context) : View(context) {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.GREEN
        strokeWidth = 4f
        textSize = 40f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas!!)
        canvas.drawLine(0f, 0f, 300f, 300f, paint)
        canvas.drawCircle(100f, 100f, 30f, paint)
        canvas.drawText("diman nubasss", 210f, 110f, paint)
    }
}