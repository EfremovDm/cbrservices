package ru.efremovdm.cbrservices

import retrofit2.Call
import ru.efremovdm.cbrservices.entity.rss.Rss
import ru.efremovdm.cbrservices.services.RssService

enum class NewsMenuItem(val title: String, val RssCall: RssService.() -> Call<Rss>) {
    NEWS("Новости Банка", RssService::getRssNews),
    PRESS("Пресс-релизы", RssService::getRssPress),
    COURSES("Курсы валют, устанавливаемые ежедневно", RssService::getRssCurrency),
    EDOC(
        "Информация выполнения регламента обработки электронных документов (МЦИ)",
        RssService::getRssMsi
    ),
    NEWS_ENG("Новое на сайте (англ.)", RssService::getEnRssNews),
    PRESS_ENG("Пресс-релизы Банка России (англ.)", RssService::getEnRssPress)
}