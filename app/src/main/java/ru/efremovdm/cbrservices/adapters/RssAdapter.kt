package ru.efremovdm.cbrservices.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import ru.efremovdm.cbrservices.R
import ru.efremovdm.cbrservices.Utils
import ru.efremovdm.cbrservices.entity.rss.RssItem

class RssAdapter( // constructor
    private val items : List<RssItem>,
    private val mOnClickListener : ListItemClickListener
) : RecyclerView.Adapter<RssAdapter.NumberViewHolder>() {

    companion object {
        private val TAG = RssAdapter::class.java.simpleName
    }

    private val viewHolderCount: Int = 0

    interface ListItemClickListener {
        fun onListItemClick(item: RssItem)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
        val context = viewGroup.context
        val layoutIdForListItem = R.layout.rss_list_item
        val inflater = LayoutInflater.from(context)
        val shouldAttachToParentImmediately = false

        val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)

        return NumberViewHolder(view)
    }

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class NumberViewHolder (
        itemView: View // constructor
    ) : RecyclerView.ViewHolder(itemView), OnClickListener  {

        var listItemNumberView: TextView
        var listItemDateView: TextView
        lateinit var item: RssItem

        init {
            listItemNumberView = itemView.findViewById<View>(R.id.tv_item_number) as TextView
            listItemDateView = itemView.findViewById<View>(R.id.item_date) as TextView
            itemView.setOnClickListener(this)
        }

        fun bind(rssItem : RssItem) {
            item = rssItem
            listItemNumberView.text = rssItem!!.title!!.toString()
            listItemDateView.text = Utils.pubDateConvert(rssItem!!.pubDate!!.toString())
        }

        override fun onClick(v: View) {
            val clickedPosition = adapterPosition
            mOnClickListener.onListItemClick(item)
        }
    }
}