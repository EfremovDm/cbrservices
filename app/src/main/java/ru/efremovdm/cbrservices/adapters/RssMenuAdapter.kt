package ru.efremovdm.cbrservices.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import ru.efremovdm.cbrservices.R

class RssMenuAdapter( // constructor
    private val items : List<String>,
    private val mOnClickListener : ListItemClickListener
) : RecyclerView.Adapter<RssMenuAdapter.NumberViewHolder>() {

    companion object {
        private val TAG = RssMenuAdapter::class.java.simpleName
    }

    private val viewHolderCount: Int = 0

    interface ListItemClickListener {
        fun onListItemClick(item: Int)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
        val context = viewGroup.context
        val layoutIdForListItem = R.layout.rss_list_item
        val inflater = LayoutInflater.from(context)
        val shouldAttachToParentImmediately = false

        val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)

        return NumberViewHolder(view)
    }

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class NumberViewHolder (
        itemView: View // constructor
    ) : RecyclerView.ViewHolder(itemView), OnClickListener  {

        var listItemNumberView: TextView

        init {
            listItemNumberView = itemView.findViewById<View>(R.id.tv_item_number) as TextView
            itemView.setOnClickListener(this)
        }

        fun bind(item : String) {
            listItemNumberView.text = item
        }

        override fun onClick(v: View) {
            mOnClickListener.onListItemClick(adapterPosition)
        }
    }
}