package ru.efremovdm.cbrservices

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.efremovdm.cbrservices.adapters.RssAdapter
import ru.efremovdm.cbrservices.app.CbrServicesApp
import ru.efremovdm.cbrservices.entity.rss.Rss
import ru.efremovdm.cbrservices.entity.rss.RssItem

class RssActivity : AppCompatActivity(), RssAdapter.ListItemClickListener {

    private var mAdapter: RssAdapter? = null
    lateinit var rssNews: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.rss_news)

        rssNews = findViewById(R.id.rss_news_items)

        val layoutManager = LinearLayoutManager(this@RssActivity)
        rssNews.layoutManager = layoutManager
        rssNews.setHasFixedSize(true)

        val arguments = intent.extras!!
        val newsRssBlockIndex = arguments.getInt("news_rss_block")

        val api = CbrServicesApp.getApi()
        val apiCall = NewsMenuItem.values().map(NewsMenuItem::RssCall)[newsRssBlockIndex]

        api.apiCall().enqueue(object : Callback<Rss> {
            override fun onResponse(call: Call<Rss>, response: Response<Rss>) {
                mAdapter = RssAdapter(response.body()!!.channel!!.itemList!!, this@RssActivity)
                rssNews.adapter = mAdapter
            }

            override fun onFailure(call: Call<Rss>, t: Throwable) {
                Toast.makeText(this@RssActivity, "Ошибка получения данных по сети!", Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onListItemClick(item: RssItem) {
        Log.d(TAG, "hello nobbbbbbssss, there is my item: ${item.link}")

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.link))
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "не найдено приложение браузера", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        private const val TAG = "RssActivity"
    }
}
