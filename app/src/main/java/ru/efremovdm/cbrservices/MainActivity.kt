package ru.efremovdm.cbrservices

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import ru.efremovdm.cbrservices.adapters.RssMenuAdapter


class MainActivity : AppCompatActivity(), RssMenuAdapter.ListItemClickListener {

    //private var mToast: Toast? = null

    ///private var mAdapter: RssMenuAdapter? = null
    //lateinit var rssMenuNews: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<RecyclerView>(R.id.news_list).apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            adapter = RssMenuAdapter(
                NewsMenuItem.values().map(NewsMenuItem::title),
                this@MainActivity
            )
        }
    }

    override fun onListItemClick(item: Int) {
        Log.d(TAG, item.toString())

        val intent = Intent(this, RssActivity::class.java)
        intent.putExtra("news_rss_block", item)

        startActivity(intent)
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}
