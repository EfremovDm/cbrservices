package ru.efremovdm.cbrservices

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        fun pubDateConvert(dateStr : String) : String {

            val format = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
            val date = format.parse(dateStr)

            val dt1 = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH)
            val dateFormatted = dt1.format(date)

            return dateFormatted
        }
    }
}